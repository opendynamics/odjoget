# ODJoget

[![CI Status](http://img.shields.io/travis/farooq82/ODJoget.svg?style=flat)](https://travis-ci.org/farooq82/ODJoget)
[![Version](https://img.shields.io/cocoapods/v/ODJoget.svg?style=flat)](http://cocoapods.org/pods/ODJoget)
[![License](https://img.shields.io/cocoapods/l/ODJoget.svg?style=flat)](http://cocoapods.org/pods/ODJoget)
[![Platform](https://img.shields.io/cocoapods/p/ODJoget.svg?style=flat)](http://cocoapods.org/pods/ODJoget)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ODJoget is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ODJoget"
```

## Author

farooq82, farooq.zaman@me.com

## License

ODJoget is available under the MIT license. See the LICENSE file for more info.
