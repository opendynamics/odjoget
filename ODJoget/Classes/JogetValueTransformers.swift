//
//  JogetDateTransform.swift
//  MSH
//
//  Created by Farooq Zaman on 25/05/2017.
//  Copyright © 2017 Persatuan Hematology Malaysia. All rights reserved.
//

import Foundation
import ObjectMapper

open class JogetDateTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = [String:String]
    
    private var key:String!
    private var dateFormat:String!
    
    public init(key:String, format:String){
        self.dateFormat = format
        self.key = key
    }
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let dateString = value as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            return dateFormatter.date(from: dateString)
        }
        
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> [String:String]? {
        if let date = value {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            let stringDate = dateFormatter.string(from: date)
            return ["column": key, "value": stringDate]
        }
        return nil
    }
}

open class StringTransform: TransformType {

    public typealias Object = String
    public typealias JSON = [String:String]
    
    private var key:String!
    
    public init(key:String)
    {
        self.key = key
    }
    
    public func transformFromJSON(_ value: Any?) -> String? {
        return value as? String
    }

    open func transformToJSON(_ value: String?) -> [String:String]? {
        if let value = value {
            return ["column": key, "value": value]
        }
        return nil
    }
}

open class BoolTransform: TransformType {
    
    public typealias Object = Bool
    public typealias JSON = [String:String]
    
    private var key:String!
    
    public init(key:String)
    {
        self.key = key
    }
    
    public func transformFromJSON(_ value: Any?) -> Bool? {
        if let value = value as? String{
            return ["1", "yes", "true"].contains(value.lowercased()) ? true : false
        }
        return false
    }
    
    open func transformToJSON(_ value: Bool?) -> [String:String]? {
        if let value = value {
            return ["column": key, "value": String(value)]
        }
        return nil
    }
}

open class IntTransform: TransformType {
    
    public typealias Object = Int
    public typealias JSON = [String:String]
    
    private var key:String!
    
    public init(key:String)
    {
        self.key = key
    }
    
    public func transformFromJSON(_ value: Any?) -> Int? {
        if let value = value as? String{
            return Int(value)
        }
        return nil
    }
    
    open func transformToJSON(_ value: Int?) -> [String:String]? {
        if let value = value {
            return ["column": key, "value": String(value)]
        }
        return nil
    }
}

open class FloatTransform: TransformType {
    
    public typealias Object = Float
    public typealias JSON = [String:String]
    
    private var key:String!
    
    public init(key:String)
    {
        self.key = key
    }
    
    public func transformFromJSON(_ value: Any?) -> Float? {
        if let value = value as? String{
            return Float(value)
        }
        return nil
    }
    
    open func transformToJSON(_ value: Float?) -> [String:String]? {
        if let value = value {
            return ["column": key, "value": String(value)]
        }
        return nil
    }
}


open class JogetURLTransform: TransformType {
    
    public typealias Object = String
    public typealias JSON = [String:String]
    
    private var key:String!
    
    public init(key:String)
    {
        self.key = key
    }
    
    public func transformFromJSON(_ value: Any?) -> String? {
        if let strURL = value as? String, strURL.characters.count > 0{
            if !(strURL.hasPrefix("http://") || strURL.hasPrefix("https://")){
                return "http://\(strURL)".addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
            }else{
                return strURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
            }
        }
        return nil
    }
    
    open func transformToJSON(_ value: String?) -> [String:String]? {
        if let value = value {
            return ["column": key, "value": String(value)]
        }
        return nil
    }
}
