//
//  StoreBinder.swift
//  1010 Store
//
//  Created by Farooq Zaman on 08/03/2017.
//  Copyright © 2017 scouffe Pvt Ltd. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import Realm
import RealmSwift
import AlamofireObjectMapper

public typealias FetchResultsHandler<T:Object> = (_ results:Results<T>?, _ error:Error?) -> (Void)
public typealias FetchFirstHandler<T:Object> = (_ result:T?, _ error:Error?) -> (Void)
public typealias LazyFetchHandler = (_ count:Int, _ error:Error?) -> (Void)


public protocol StoreBinder:Mappable{
    static func store(realm:Realm, records:[Mappable], filter:NSPredicate?)
    static func markDirty(realm:Realm, predicate:NSPredicate)
}

public func fetch<K:Object, R:StoreBinder>(for url:URL, type:R.Type, realm:Realm, filter:NSPredicate? = nil, payload:[String:Any]? = nil, handler:FetchResultsHandler<K>? = nil){
    
    var request:DataRequest!
    if let payload = payload{
        request = Alamofire.request(url, method: .post, parameters: payload, encoding: JSONEncoding.default)
    }else{
        request = Alamofire.request(URLRequest(url: url))
    }
    
    let utilityQueue = DispatchQueue.global(qos: .utility)
    request.responseArray(queue: utilityQueue) { (response: DataResponse<[R]>) in
        DispatchQueue.main.async{
            switch(response.result){
            case .failure(let error):
                handler?(nil, error)
            case .success(let records):
                R.store(realm:realm, records: records, filter: filter)
                if let filter = filter{
                    let results = realm.objects(type as! K.Type).filter(filter)
                    handler?(results, nil)
                }
            }
        }
    }
}

public func lazyFetch<R:StoreBinder>(for url:URL, type:R.Type, realm:Realm, filter:NSPredicate? = nil, payload:[String:Any]? = nil, handler:@escaping LazyFetchHandler){
    
    var request:DataRequest!
    if let payload = payload{
        request = Alamofire.request(url, method: .post, parameters: payload, encoding: JSONEncoding.default)
    }else{
        request = Alamofire.request(URLRequest(url: url))
    }
    
    let utilityQueue = DispatchQueue.global(qos: .background)
    
    request.responseArray(queue: utilityQueue){ (response: DataResponse<[R]>) in
        DispatchQueue.main.async{
            switch(response.result){
            case .success(let records):
                R.store(realm:realm, records: records, filter: filter)
            default:break
            }
        }
        }.responseJSON{response in
            switch response.result{
            case .failure(let error):
                handler(0, error)
            case .success(let records):
                handler((records as? [[String:Any]])?.count ?? 0, nil)
            }
    }
}
