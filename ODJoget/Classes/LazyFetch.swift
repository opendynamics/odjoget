//
//  LazyFetch.swift
//  NearSnappIOS
//
//  Created by Farooq Zaman on 25/03/2017.
//  Copyright © 2017 MatrixCentric. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

extension Notification.Name{
    static public let allPagesFetchedNotification = Notification.Name("allPagesFetchedNotification")
    static public let singlePageFetchedNotification = Notification.Name("singlePageFetchedNotification")
    static public let pageFetchErrorNotification = Notification.Name("pageFetchErrorNotification")
}

public typealias LazyFetchCompletionHandler = () -> (Void)

open class LazyFetch<T:StoreBinder>{
    private var index:Int = 0
    private var pageSize:Int = 0
    private var baseURL:URL!
    private var filter:NSPredicate!
    private var shouldStop:Bool = false
    private var completionHadler:LazyFetchCompletionHandler!
    
    public init(pageSize:Int, baseURL:URL, filter:NSPredicate, completion:@escaping LazyFetchCompletionHandler) {
        self.pageSize = pageSize
        self.baseURL = baseURL
        self.filter = filter
        self.completionHadler = completion
    }
    
    open func start(){
        T.markDirty(realm: ODJoget.shared.realm, predicate: filter)
        fetchData(startIndex: index, count: pageSize)
    }
    
    open func stop(){
        shouldStop = true
    }
    
    private func fetchData(startIndex:Int, count:Int){
        let url = URL(string:"\(baseURL.absoluteString)&start=\(startIndex)&recotd=\(count)")!
        
        lazyFetch(for: url, type: T.self, realm: ODJoget.shared.realm) { (count, error) -> (Void) in
            if error != nil{
                NotificationCenter.default.post(name: .pageFetchErrorNotification, object:error)
                self.completionHadler()
                return
            }
            
            if count == self.pageSize && !self.shouldStop{
                self.fetchData(startIndex: startIndex + count, count: count)
                NotificationCenter.default.post(name: .singlePageFetchedNotification, object: String(describing:T.self))
            }else{
                NotificationCenter.default.post(name: .allPagesFetchedNotification, object: String(describing:T.self))
                self.completionHadler()
            }
        }
    }
}
