//
//  API.swift
//  Pods
//
//  Created by Farooq Zaman on 08/05/2017.
//
//

import Foundation
import Realm
import RealmSwift

public typealias APIConfigurator = ()->(url:URL, predicate:NSPredicate)
public typealias APIConfiguratorWithPayload = ()->(url:URL, predicate:NSPredicate, payload:[String:Any])

open class JogetAPI<R:StoreBinder, K:Object>{
    var url:URL?
    var predicate:NSPredicate?
    var lastFetched:Date?
    var error:Error?
    var payload:[String:Any]?
    
    public init(url:URL, predicate:NSPredicate, payload:[String:Any]? = nil){
        self.url = url
        self.predicate = predicate
        self.payload = payload
    }
    
    public init(_ configure: APIConfigurator ) {
        let (url, predicate) = configure()
        self.url = url
        self.predicate = predicate
    }
    
    public init(_ configure: APIConfiguratorWithPayload ) {
        let (url, predicate, payload) = configure()
        self.url = url
        self.predicate = predicate
        self.payload = payload
    }
    
    private func load(completion:FetchResultsHandler<K>? = nil){
        fetch(for: url!, type: R.self, realm: ODJoget.shared.realm, filter: predicate, payload:payload) { (result:Results<K>?, error:Error?) -> (Void) in
            if error == nil{
                self.lastFetched = Date()
            }else{
                self.lastFetched = nil
                self.error = error
            }
            completion?(result, error)
        }
    }
    
    public func first(completion:FetchFirstHandler<K>? = nil) {
        if lastFetched == nil || error != nil{
            load(completion: { (results:Results<K>?, error:Error?) -> (Void) in
                completion?(results?.first, error)
            })
        }else{
            completion?(ODJoget.shared.realm.objects(K.self).filter(predicate!).first, nil)
        }
    }
    
    public func firstSynced(completion:FetchFirstHandler<K>? = nil){
        load(completion: { (results:Results<K>?, error:Error?) -> (Void) in
            completion?(results?.first, error)
        })
    }
    
    public func firstIfNotExists(completion:FetchFirstHandler<K>? = nil){
        if let first = ODJoget.shared.realm.objects(K.self).filter(predicate!).first{
            completion?(first, nil)
        }else{
            load(completion: { (results:Results<K>?, error:Error?) -> (Void) in
                completion?(results?.first, error)
            })
        }
    }
    
    public func all(completion:FetchResultsHandler<K>? = nil){
        if lastFetched == nil || error != nil{
            load(completion: { (results:Results<K>?, error:Error?) -> (Void) in
                completion?(results, error)
            })
        }else{
            completion?(ODJoget.shared.realm.objects(K.self).filter(predicate!), nil)
        }
    }
    
    public func allSynced(completion:FetchResultsHandler<K>? = nil){
        load(completion: { (results:Results<K>?, error:Error?) -> (Void) in
            completion?(results, error)
        })
    }
}
