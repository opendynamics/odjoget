//
//  ODJoget.swift
//  Pods
//
//  Created by Farooq Zaman on 08/05/2017.
//
//

import Foundation
import Realm
import RealmSwift

open class ODJoget{
    open static let shared = ODJoget()
    var realmInst:Realm?
    
    var realm:Realm{
        get{
            assert(realmInst != nil, "ODJoget requires realm to function. Use ODJoget.shared.with(realm:) to provide realm reference.")
            return realmInst!
        }
    }
    
    open func with(realm:Realm){
        self.realmInst = realm
    }
}
